package com.example.springboot.repository;

import com.example.springboot.entity.Receptionist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReceptionistRepository extends JpaRepository<Receptionist, Integer> {
    Receptionist findBydaftarTamu(String daftarTamu);
    Receptionist findByPerusahaan(String perusahaan);
}