package com.example.springboot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "receptionist")
public class Receptionist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer idReceptionist;

    @Column(nullable = false)
    private String daftarTamu;

    @Column(nullable = false)
    private String perusahaan;

    @Column(nullable = false)
    private String picPerusahaan;

    @Column(nullable = false)
    private String jadwalMeeting;

//    @Column(nullable = false)
//    private String registrasiTamu;

    public Integer getIdReceptionist() {
        return idReceptionist;
    }

    public void setIdReceptionist(Integer idReceptionist) {
        this.idReceptionist = idReceptionist;
    }

    public String getPerusahaan() {
        return perusahaan;
    }

    public void setPerusahaan(String perusahaan) {
        this.perusahaan = perusahaan;
    }

    public String getPicPerusahaan() {
        return picPerusahaan;
    }

    public void setPicPerusahaan(String picPerusahaan) {
        this.picPerusahaan = picPerusahaan;
    }

    public String getJadwalMeeting() {
        return jadwalMeeting;
    }

    public void setJadwalMeeting(String jadwalMeeting) {
        this.jadwalMeeting = jadwalMeeting;
    }

    public String getDaftarTamu() {
        return daftarTamu;
    }

    public void setDaftarTamu(String daftarTamu) {
        this.daftarTamu = daftarTamu;
    }

    //    public String getRegistrasiTamu() {
//        return registrasiTamu;
//    }
//
//    public void setRegistrasiTamu(String registrasiTamu) {
//        this.registrasiTamu = registrasiTamu;
//    }
}
