package com.example.springboot.service;

import com.example.springboot.entity.Receptionist;
import com.example.springboot.repository.ReceptionistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReceptionistService {
    @Autowired
    private ReceptionistRepository receptionisRepository;

    public Receptionist saveTamu(Receptionist receptionis) {
        return receptionisRepository.save(receptionis);
    }


    public List<Receptionist> getTamu() {
        return receptionisRepository.findAll();
    }


    public String deleteTamu(int id) {
        receptionisRepository.deleteById(id);
        return "Tamu Removed";
    }


    public Receptionist updateTamu(Receptionist receptionis) {
        Receptionist existingTamu = (Receptionist) receptionisRepository.findById(receptionis.getIdReceptionist()).orElse(null);
        existingTamu.setDaftarTamu(receptionis.getDaftarTamu());
        existingTamu.setPerusahaan(receptionis.getPerusahaan());
        existingTamu.setPicPerusahaan(receptionis.getPicPerusahaan());
        existingTamu.setJadwalMeeting(receptionis.getJadwalMeeting());
        return (Receptionist) receptionisRepository.save(existingTamu);
    }

    public Optional<Receptionist> findById(int id){
        return receptionisRepository.findById(id);
    }


    public Receptionist getReceptionistByNamaTamu(String daftarTamu) {
        return receptionisRepository.findBydaftarTamu(daftarTamu);
    }

    public Receptionist getReceptionistByPerusahaan(String perusahaan) {
        return receptionisRepository.findByPerusahaan(perusahaan);
    }
}
