package com.example.springboot.controller;

import com.example.springboot.entity.Receptionist;
import com.example.springboot.service.ReceptionistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RestController
    public class ReceptionistController {
        @Autowired
        private ReceptionistService receptionisService;

        //AddTamu
        @PostMapping("/addTamu")
        public Receptionist addTamu(@RequestBody Receptionist receptionis){
            return receptionisService.saveTamu(receptionis);
        }

        //GetTamu
        @GetMapping("/tamu")
        public List<Receptionist> findAllTamu(){
            return receptionisService.getTamu();
        }

        //DeleteTamu
        @DeleteMapping("/delete/{id}")
        public String deleteTamu(@PathVariable int id){
            return receptionisService.deleteTamu(id);
        }

        //UpdateTamu
        @PutMapping("/update/{id}")
        public Receptionist updateReceptionis(@RequestBody Receptionist receptionis, @PathVariable int id){
            receptionis.setIdReceptionist(id);
            return receptionisService.updateTamu(receptionis);
        }

        //FindById
        @GetMapping("/tamu/{id}")
        public Optional<Receptionist> findById(@PathVariable int id){
            return receptionisService.findById(id);
        }

        //FindByName
        @GetMapping("/tamu/{daftarTamu}")
        public Receptionist findByDaftarTamu(@PathVariable String daftarTamu){
            return receptionisService.getReceptionistByNamaTamu(daftarTamu);
        }

        //FinByPerusahan
        @GetMapping("/perusahaan/{perusahaan}")
        public Receptionist findByPerusahaan(@PathVariable String perusahaan){
            return receptionisService.getReceptionistByPerusahaan(perusahaan);
        }
    }
